<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');
Route::post('/password/email','Api\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset','Api\ResetPasswordController@reset');


Route::group(['middleware' => 'auth:api'], function() {
    
    Route::get('/user', 'Api\UserController@showDetails');

    Route::get('/test',function(){
        if (Auth::check()) {
            return 'Yes, You are logged in!';
        } else {
            return 'User is logged out';
        }
    });

    Route::post('logout', 'Api\AuthController@logout');

    
});



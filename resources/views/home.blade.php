@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> -->
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>
                        <b>
                            USERS CONTROLLER API
                        </b>
                    </h3>
                </div>

                <div class="card-body">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#register" aria-expanded="false" aria-controls="register">
                                        <b>Register User</b> 
                                    </button>
                                </h2>
                            </div>
                            <div id="register" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card card-body">
                                    <span> HTTP Method: <b>POST</b> </span>
                                    <span> URL: "<b>/api/register</b>" </span>
                                    <span> 
                                        Request Body: <br>
                                        <b>
                                        {<br>
                                            "first_name": "string", <br>
                                            "last_name": "string", <br>
                                            "email": "email", <br>
                                            "password": "string", <br>
                                            "password_confirmation": "string" <br>
                                        }
                                        </b>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#login" aria-expanded="false" aria-controls="login">
                                        <b>Login User</b> 
                                    </button>
                                </h2>
                            </div>
                            <div id="login" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card card-body">
                                    <span> HTTP Method: <b>POST</b> </span>
                                    <span> URL: "<b>/api/login</b>" </span>
                                    <span> 
                                        Request Body: <br>
                                        <b>
                                        {<br>
                                            "first_name": "string", <br>
                                            "last_name": "string", <br>
                                            "email": "email", <br>
                                            "password": "string", <br>
                                            "password_confirmation": "string" <br>
                                        }
                                        </b>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <b>Show User Details</b> 
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card card-body">
                                    <span> HTTP Method: <b>GET</b> </span>
                                    <span> URL: "<b>/api/user</b>" </span>
                                    <span> 
                                        Request Body: none
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#register" aria-expanded="false" aria-controls="register">
                            Register User API
                        </button>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#login" aria-expanded="false" aria-controls="register">
                            Login User API
                        </button>
                    </p>
                    <div class="collapse" id="register">
                        <div class="card card-body">
                            <span> HTTP Method: <b>POST</b> </span>
                            <span> URL: "<b>/register</b>" </span>
                            <span> 
                                Request Body: <br>
                                <b>
                                {<br>
                                    "first_name": "string", <br>
                                    "last_name": "string", <br>
                                    "email": "email", <br>
                                    "password": "string", <br>
                                    "password_confirmation": "string" <br>
                                }
                                </b>
                            </span>
                        </div>
                    </div>
                    <div class="collapse" id="login">
                        <div class="card card-body">
                            <span> HTTP Method: <b>POST</b> </span>
                            <span> URL: "<b>/login</b>" </span>
                            <span> 
                                Request Body: <br>
                                <b>
                                {<br>
                                    "first_name": "string", <br>
                                    "last_name": "string", <br>
                                    "email": "email", <br>
                                    "password": "string", <br>
                                    "password_confirmation": "string" <br>
                                }
                                </b>
                            </span>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

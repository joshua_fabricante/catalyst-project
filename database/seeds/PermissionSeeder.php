<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DEFINED PERMISSIONS
        DB::table('permissions')->insert([
            'description' => 'Create User',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'description' => 'Assign Roles',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'description' => 'Create Blog Post',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'description' => 'Update Draft Blog Post',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'description' => 'Approve Blog Post',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'description' => 'Read-only View',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'description' => 'View Blog Post',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}

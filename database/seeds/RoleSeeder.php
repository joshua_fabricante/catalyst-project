<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'title' => 'Superadmin',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
            'title' => 'Admin',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
            'title' => 'User',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        DB::table('roles')->insert([
            'title' => 'Approver',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
            'title' => 'Reviewer',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
            'title' => 'Read-Only',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        // ROLE - PERMISSION
        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 2,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 2,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 3,
            'role_id' => 3,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 4,
            'role_id' => 3,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 5,
            'role_id' => 4,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 6,
            'role_id' => 5,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 7,
            'role_id' => 6,
        ]);
    }
}

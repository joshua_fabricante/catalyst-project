<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DEFINED USERS
        DB::table('users')->insert([
            'first_name' => 'Joshua Irving',
            'last_name' => 'Fabricante',
            'email' => 'joshua.starkiller115@gmail.com',
            'password' => bcrypt('adminadmin'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'first_name' => 'Fatima',
            'last_name' => 'Lazan',
            'email' => 'fatima615.lazan@gmail.com',
            'password' => bcrypt('adminadmin'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        // USER - ROLE
        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 1,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 2,
        ]);
    }
}

<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->belongsToMany(Role::class, 'role_user');
    }

      /** 
     * @param string|array $roles
     */
    public function authorizeRoles($roles) {
        if (is_array($roles)) {
            if ($this->hasAnyRole($roles)) {
                return true;
            }
            return false;
        }

        if ($this->hasRole($roles)) {
            return true;
        }
        return false;
    }

     /** 
     * Check multiple roles
     * 
     * @param array $roles
     */
    public function hasAnyRole($roles) {
        if ($this->role()->whereIn('role_id', $roles)->first()) {
            return true;
        }
        return false;
    }
    
    /**
     * Check one role
     * 
     * @param string $role
     */
    public function hasRole($role) {
        if ($this->role()->where('role_id', $role)->first()) {
            return true;
        }
        return false;
    }

    /**
     * Check if user is authorized for the request
     * 
     * @param int $user_id
     */
    public function verifyUser ($user_id) {
        if (is_array($user_id)) {
            foreach ($user_id as $id) {
                $user = Auth::user()->find($id);

                if ($user->role()->where('role_id', 1)->first()) {
                    return false;
                } else {
                    if (!$this->verifyUserOrg($user->organization_id)) {
                        return false;
                    }
                }
            }
            return true;
        }
        if ($this->id == $user_id) {
            return true;
        }
        return false;
    }

    /**
     * Check if user is authorized for the request
     * 
     * @param int $organization_id
     * @param int $role_id
     */
    public function verifyIfUserAdmin ($organization_id, $role_id) {
        if ($role_id == 2 && $this->role()->where('role_id', $role_id)->first()) {
            if ($this->verifyUserOrg($organization_id)) {
                return true;
            }
        }
        return false;
    }

      /**
     * Check if user is authorized for the request
     * 
     * @param int $role_id
     */
    public function verifyUserRole ($role_id) {
        if ($this->role()->where('role_id', $role_id)->first()) {
            return true;
        }
        return false;
    }
}

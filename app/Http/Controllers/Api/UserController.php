<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    const SUCCESS_STATUS = 200;
    const BAD_REQUEST_STATUS = 400;
    const UNAUTHORIZED_STATUS = 401;

    // Assign role to an existing user
    public function assignRole(Request $request, $user_id) {
        $validated = $request->validated(); //validation

        $user = Auth::user()->find($user_id);
        
        if (Auth::user()->verifyUserRole($request->query('role'))) {
            if ($request->query('role') == 1) { // superadmin
                // do nothing and proceed
            } else if ($request->query('role') == 2) { // admin
                if ($user->role()->where('role_id', 1)->first()) {
                    return response()->json([
                        'success' => false,
                        'message' => trans('custom.access_denied')
                    ], self::UNAUTHORIZED_STATUS);
                } else {
                    if (!Auth::user()->verifyUserOrg($user->organization_id)) {
                        return response()->json([
                            'success' => false,
                            'message' => trans('custom.access_denied')
                        ], self::UNAUTHORIZED_STATUS); 
                    }
                }
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => trans('custom.access_denied')
            ], self::UNAUTHORIZED_STATUS); 
        }

        if ($request->has('role_id')) {
            $input = $request->input();

            if($request->filled('role_id')) {
                $input['role_id'] = $request->request->get('role_id'); 
            }

            if (!$user->role()->sync(array_filter($input['role_id']))) {
                return response()->json([
                    'success' => false,
                    'message' => trans('custom.user.assign_role.failed')
                ], self::INTERNAL_SERVER_STATUS);
            }

            return response()->json([
                'success'=> true,
                'message'=> trans('custom.user.assign_role.success')
            ], self::SUCCESS_STATUS);
        } else {
            return response()->json([
                'success' => false,
                'message' => trans('custom.invalid_input')
            ], self::BAD_REQUEST_STATUS);
        }
    }
    
    public function showDetails() {
        $user = Auth::user();

        $roles = $user->role;

        $user['address'] = $user->address;
        unset($user['address_id']);

        foreach($roles as $role) {
            $role['permission'] = $role->permission;
        }
        
        $user = $user->toArray();
        $user['role'] = $roles;

        return response()->json(['success' => $user], self::SUCCESS_STATUS);
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Controllers\Controller; 
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    const SUCCESS_STATUS = 200;
    const BAD_REQUEST_STATUS = 400;
    const UNAUTHORIZED_STATUS = 401;

    // Register User
    public function register(UserRegisterRequest $request){

        // return $request->request->get('first_name');
        $validateData = $request->validate([
            'first_name'=>'required|max:55',
            'last_name'=>'required|max:55',
            'email'=>'email|required|unique:users',
            'password'=>'required|confirmed'
        ]);
        

        if($validateData){
            $validateData['password'] = bcrypt($request->password);
            $user = User::create($validateData);
            $accessToken = $user->createToken('authToken')->accessToken;
            return response()->json([
                'success' => true,
                'message' => 'You have successfully registered a user!'
            ], self::SUCCESS_STATUS);
            // return response(['user'=>$user,'access_token'=>$accessToken]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Bad Request'
        ], self::BAD_REQUEST_STATUS);
        
        
    }


    // Basic Login User
    public function login(UserLoginRequest $request){
        $loginCreds = $request->validate([
            'email'=>'email|required',
            'password'=>'required'
        ]);

        if(!auth()->attempt($loginCreds) ){
            return response([
                'success' => false,
                'message'=>'Invalid Credentials']);
        } else {

            $accessToken = 'Bearer ' . auth()->user()->createToken('AuthToken')->accessToken;
            return response()->json([
                'success' => true,
                'message' => 'You have successfully logged in!',
                'access_token' => $accessToken
            ], self::SUCCESS_STATUS);
            // return response(['access_token' => $accessToken]);
        }

    }
    // Login User
    // public function login(UserLoginRequest $request) {
    //     $validated = $request->validated(); //validation
        
    //     $input = $request->input();

    //     $input['email'] = $request->request->get('email');
    //     $input['password'] = $request->request->get('password');
        
    //     if (Auth::attempt(['email' => $input['email'], 'password' => $input['password']])) {
    //         $user = Auth::user();
    //         $roles;
            
    //         foreach($user->role as $role) {
    //             $roles = $role->pivot->role_id;
    //         }

    //         if (!empty($roles)) { 
    //             $success['token'] =  'Bearer ' . $user->createToken('ICAO')->accessToken; 
    //             return response()->json(['success' => $success], self::SUCCESS_STATUS); 
    //         } else {
    //             return response()->json([
    //                 'success' => false,
    //                 'message' => 'Access Denied'
    //             ], self::UNAUTHORIZED_STATUS);
    //         }
    //     } else {
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Access Denied'
    //         ], self::UNAUTHORIZED_STATUS);
    //     }
    // }

    public function logout() {
        $accessToken = Auth::user()->token();
        $accessToken->revoke();
    
        return response()->json([
            'success' => true,
            'message' => 'You have successfully Logged-out'
        ], self::SUCCESS_STATUS);
    }
}

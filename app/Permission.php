<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// Swagger Annotation for Permission-Schema
/**
 *  @OA\Schema(
 *      @OA\Property(property="id", type="integer"),
 *      @OA\Property(property="description", type="string")
 * )
 */
class Permission extends Model
{
    use SoftDeletes;

    public $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function role() {
        return $this->belongsToMany(Role::class);
    }
}

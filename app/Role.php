<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// Swagger Annotation for Role-Schema
/**
 *  @OA\Schema(
 *      @OA\Property(property="id", type="integer"),
 *      @OA\Property(property="title", type="string")
 * )
 */
class Role extends Model
{
    use SoftDeletes;

    public $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function user() {
        return $this->belongsToMany(User::class);
    }

    public function permission() {
        return $this->belongsToMany(Permission::class);
    }
}
